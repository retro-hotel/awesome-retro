<div align="center">

![Awesome Retro Banner](images/banner.png)

</div>

# Awesome Retro: Plugins
A curated and collective collection of awesome Retro Hotel plugins and websites related to plugins.

## Contents
- [Awesome Retro: Plugins](#awesome-retro-plugins)
  - [Contents](#contents)
  - [Furniture and Decoration Plugins](#furniture-and-decoration-plugins)
  - [Mini Game and Entertainment Plugins](#mini-game-and-entertainment-plugins)
  - [Security and Moderation Plugins](#security-and-moderation-plugins)
  - [User Interface and Experience](#user-interface-and-experience)
  - [Currency and Economy Plugins](#currency-and-economy-plugins)
  - [Community and Social Plugins](#community-and-social-plugins)
  - [Avatar and Clothing Plugins](#avatar-and-clothing-plugins)
  - [Events and Competitions Plugins](#events-and-competitions-plugins)
  - [Quests and Achievements Plugins](#quests-and-achievements-plugins)
  - [Chat and Communication Plugins](#chat-and-communication-plugins)
  - [Media and Entertainment Integration](#media-and-entertainment-integration)
  - [Scripting and Development Resources](#scripting-and-development-resources)
  - [Plugin Compatibility and Version](#plugin-compatibility-and-version)
  - [Plugin Development Community](#plugin-development-community)

## Furniture and Decoration Plugins
Plugins that add new furniture items, decorative elements, and room customization options to Retro Habbo Hotels.

- [MS3] [Build Height](https://git.krews.org/morningstar/archive/build-height) - Adds a floorheight/buildheight command
- [MS3] [Custom Wired](https://git.krews.org/morningstar/archive/custom-wired) - Custom Wired

## Mini Game and Entertainment Plugins
Plugins that introduce mini-games, entertainment features, or interactive experiences within the Retro Habbo environment.

- [MS3] [Fun Commands](https://git.krews.org/morningstar/archive/fun-commands) - Fun Custom Commands

## Security and Moderation Plugins
Plugins designed to enhance security, prevent cheating, and assist with moderation tasks in Retro Habbo Hotels.

-

## User Interface and Experience
Plugins that improve the user interface, navigation, and overall experience for players in the retro hotel.

-

## Currency and Economy Plugins
Plugins related to in-game currency, trading systems, and economic aspects of Retro Habbo Hotels.

-

## Community and Social Plugins
Plugins that facilitate communication, social interaction, and community-building among retro hotel users.

-

## Avatar and Clothing Plugins
Plugins that offer new avatar customization options, clothing items, and appearance enhancements.

-

## Events and Competitions Plugins
Plugins designed to create, manage, and promote in-game events, competitions, and challenges.

-

## Quests and Achievements Plugins
Plugins that introduce quests, achievements, and progression systems to enhance gameplay.

- [MS] [LevelSystem-Plugin](https://github.com/Eyupc/LevelSystem-Plugin) - LevelSystem plugin for Arcturus Morningstar

## Chat and Communication Plugins
Plugins focused on chat functionality, messaging systems, and communication tools within Retro Habbo Hotels.

-

## Media and Entertainment Integration
Plugins that integrate media, music, videos, or external entertainment sources into the retro hotel experience.

- [MS3] [Apollyon](https://git.krews.org/morningstar/archive/apollyon) - Apollyon is a Camera Plugin for Arcturus Morningstar

## Scripting and Development Resources
Resources, tutorials, and tools for developers interested in creating their own plugins for Retro Habbo Hotels.

-

## Plugin Compatibility and Version
Information and guides regarding which plugins are compatible with specific Retro Hotel emulators and versions.

-

## Plugin Development Community
Resources and forums for plugin developers to collaborate, share ideas, and seek assistance within the Retro Habbo community.

-