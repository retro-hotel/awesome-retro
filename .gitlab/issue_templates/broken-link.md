---
name: Broken Link Report
about: Create a report of a broken link
---

Be sure to check the existing issues (both open and closed!).  
Make sure you follow the checklist to know for sure the link is actually broken.

## Reporting a broken URL

- **URL**:
- **Error** (optional):

### Checklist

* [ ] **Reload the Page.**  
Sometimes, a website may experience temporary issues. Try refreshing the page to see if the link becomes active again.

* [ ] **Use a Different Browser.**  
Test the link using a different web browser. Sometimes, browser-specific issues can cause a link to appear broken in one browser but work in another.

* [ ] **Try on a Different Device.**  
Test the link on a different device, such as a smartphone or tablet, to see if the issue persists across multiple devices.

* [ ] **Check for Server Errors.**  
Look for error messages that the website may provide when the link is accessed. Common server errors include "404 Not Found" or "503 Service Unavailable."

* [ ] **Disconnect VPN or Proxy Connection.**  
If you're using a VPN or proxy service to access the internet, it's possible that the link might appear broken because of the specific server you're connected to. Try disconnecting from the VPN or proxy and accessing the link directly to see if it works without them.