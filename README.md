<div align="center">

![Awesome Retro Banner](images/banner.png)

</div>

# Awesome Retro
A curated and collective collection of awesome Retro Hotel resources, websites and more.

## Contents
- [Awesome Retro](#awesome-retro)
  - [Contents](#contents)
  - [Introduction to Retro Habbo Hotels](#introduction-to-retro-habbo-hotels)
  - [Retro Hotel Emulators and Other Tools](#retro-hotel-emulators-and-other-tools)
    - [Emulators](#emulators)
    - [Frontend and Backend](#frontend-and-backend)
    - [Clients](#clients)
    - [Imagers](#imagers)
  - [Build and Manage Your Retro Hotel](#build-and-manage-your-retro-hotel)
  - [Customization and Theming](#customization-and-theming)
  - [Retro Hotel Community](#retro-hotel-community)
  - [Furniture and Rares](#furniture-and-rares)
  - [Security and Moderation](#security-and-moderation)
  - [Events and Competitions](#events-and-competitions)
  - [Retro Hotel Addons, Script and Plugins](#retro-hotel-addons-script-and-plugins)
  - [Graphic Design Resources](#graphic-design-resources)
  - [Retro Hotel News and Updates](#retro-hotel-news-and-updates)
  - [Other Retro Hotel Communities](#other-retro-hotel-communities)


## Introduction to Retro Habbo Hotels
An introduction to what Retro Habbo Hotels are and why they are popular.

- [Habbo Retro on Habbo Fandom](https://habbo.fandom.com/wiki/Habbo_Retro)
- [Retro Hotels on Habbox Wiki](https://www.habboxwiki.com/Retro_Hotels)

## Retro Hotel Emulators and Other Tools
A list of emulation software and tools that allow you to host your own retro hotel.

### Emulators
- [Arcturus Morningstar](https://git.krews.org/morningstar/Arcturus-Community) - An open-source Arcturus Community Fork
- [Comet v2](https://git.krews.org/duckietm/comet-v2) - Comet is a high performance game server written in Java.
- [Kepler](https://github.com/Quackster/Kepler) - A Habbo Hotel v14 server written in Java
- [Havana](https://github.com/Quackster/Havana) - A server created in Java designed to revive Habbo Hotel from the 2009 era

### Frontend and Backend
- [CMS] [Atom CMS](https://github.com/ObjectRetros/atomcms) - Atom CMS is an open source Retro CMS
- [CMS] [Hylib](https://github.com/TheLaxus/Hylib) - A CMS developed in ReactJS using NodeJS as backend for Comet
- [HK] [Atom HK](https://github.com/ObjectRetros/atomhk) - Atom Housekeeping is a standalone housekeeping for Habbo retros
- [CMS + HK] [Celestial](https://git.krews.org/Laura/celestial) - A modern Laravel/Vue based retro CMS

### Clients
- [Nitro] [Nitro React](https://github.com/billsonnn/nitro-react) - Nitro React Client

### Imagers
- [nitro-imager](https://github.com/billsonnn/nitro-imager) - Server-side Habbo Imager written in Typescript
- [docker-imager](https://git.krews.org/duckietm/docker-imager) - Habbo Imager written in Typescript and is Docker ready

## Build and Manage Your Retro Hotel
Guides and tutorials on setting up and managing a Retro Habbo Hotel.

- [Atom CMS Tutorials](https://retros.guide/docs/atom-cms/introduction) - Collections of tutorials written for Atom CMS
- [Atom HK Tutorials](https://retros.guide/docs/atom-hk/introduction) - Collections of tutorials written for Atom HK

## Customization and Theming
Resources and tips for customizing and theming your retro hotel.

- [HabboAssets](https://www.habboassets.com/) - Large collection of up-to-date Habbo assets
- [Habbo Font](https://www.dafont.com/habbo.font) - One of several Habbo fonts (download)

## Retro Hotel Community
Links to forums, communities, and social media groups dedicated to Retro Habbo Hotels.

- [Krews](https://git.krews.org/) - GitLab instance related to Habbo Retro
- [DevBest - Habbo Retro](https://devbest.com/forums/habbo-retros.30/) - Habbo Retro section on DevBest forums
- [RaGEZONE - Habbo Hotel](https://forum.ragezone.com/community/habbo-hotel.282/) - Habbo section on RaGEZONE forums
- [FindRetros](https://findretros.com/) - Find the best Habbo retros in town
- [VindRetros](https://vindretros.nl/) - Find Dutch Habbo retros hotels

## Furniture and Rares
Guides for creating and adding custom furniture and rare items to your retro hotel.

- [Furnibuilder.com](https://www.furnibuilder.com/) - Build your own furniture online for free!

## Security and Moderation
Information on securing your retro hotel and implementing moderation.

-

## Events and Competitions
Ideas and tools for organizing events and competitions within your retro hotel.

-

## Retro Hotel Addons, Script and Plugins
Useful add-ons, scripts and plugins to extend features and interactions in your retro hotel.

- **[Awesome Retro: Plugins](PLUGINS.md) ⭐ - A curated and collective collection of awesome Retro Hotel plugins**
- [Plugin MS] [NitroWebsockets](https://git.krews.org/morningstar/nitrowebsockets-for-ms) - NitroWebsockets is a plugin that adds Nitro HTML5 Client compatibility
- [Plugin MS3] [Apollyon](https://git.krews.org/morningstar/archive/apollyon) - A PNGCamera for MS 3.x

## Graphic Design Resources
Links to graphic design tools and resources for creating custom images and avatars.

- [HabboFont](https://habbofont.net/) - Simply generate Habbo styled texts

## Retro Hotel News and Updates
Websites and sources that share news and updates about the Retro Habbo Hotel community.

-

## Other Retro Hotel Communities
List of other Retro Hotel servers and communities you can explore.

-